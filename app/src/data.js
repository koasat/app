const data = [
  {
    name: 'ti9 final game',
    videoId: 'wucMZbj7lj8',
    type: ['dota']
  },
  {
    name: '17th wkc brazil vs serbia',
    videoId: 'BBND7GwMsM8',
    type: ['kendo']
  },
  {
    name: 'dog training',
    videoId: '9UwXfFJ6JzU',
    type: ['dogs']
  },
  {
    name: 'dota dog predictions',
    videoId: 'fg7zQva6lAE',
    type: ['dota', 'dogs']
  },
  {
    name: 'slicing fruits',
    videoId: 'VjINuQX4hbM',
    type: ['food']
  }
]
export default data
